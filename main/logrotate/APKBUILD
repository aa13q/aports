# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=logrotate
pkgver=3.18.1
pkgrel=1
pkgdesc="Tool to rotate logfiles"
url="https://github.com/logrotate/logrotate"
arch="all"
license="GPL-2.0-or-later"
makedepends="popt-dev autoconf automake libtool"
checkdepends="coreutils"
subpackages="$pkgname-doc $pkgname-openrc"
source="$url/releases/download/$pkgver/$pkgname-$pkgver.tar.xz
	logrotate.cron
	logrotate.conf
	logrotate.confd"

prepare() {
	default_prepare

	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -dm755 "$pkgdir"/etc/logrotate.d/
	install -Dm644 ../logrotate.conf \
		"$pkgdir"/etc/logrotate.conf

	install -Dm755 "$srcdir"/logrotate.cron \
		"$pkgdir"/etc/periodic/daily/logrotate
	install -Dm644 "$srcdir"/logrotate.confd  \
		"$pkgdir"/etc/conf.d/logrotate
}

sha512sums="
d559bf188f587096433887d3a89040fa82f4db35fbae84a9e6d04c425e6d004cbed9bd48bb3eaf53e424a82e1e777b02e55ee17ecdb4c6c0ec3db81964db5b14  logrotate-3.18.1.tar.xz
f4d708594fb2b240cfc2928f38a180d27c2cecb9867e048dc29a32c0147244db4d2f6d92e7bff27e1f2623537587db87b2f8fc9bb988f98eff0c98f79f5a5bf2  logrotate.cron
e91c1648a088410d1f5ad16d05b67e316977be5cc0cbbb21a4e1fda2267415fb7945553aa4b4a4701d658fd6bfe35e3d9a304e0cf2a9c7f1be5a5753c3dbc7cb  logrotate.conf
be9f0043b594d26b4f64e07a2188d19c3c43af75ef726305e4d98f744fc16cee9f280227116858e2f5b781c0a7b58e0209d7e9ab1285dfa7ba55a9dfda700229  logrotate.confd
"
