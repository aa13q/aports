# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swayr
pkgver=0.12.1
pkgrel=0
pkgdesc="A window switcher (and more) for Sway"
url="https://sr.ht/~tsdh/swayr/"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="GPL-3.0-or-later"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~tsdh/swayr/archive/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare

	# Optimize binary for size.
	cat >> Cargo.toml <<-EOF

		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "z"
		panic = "abort"
	EOF

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
c9d0dd39a6b92753f6dd4f6f9bdd10f5c13c6c616a007673b874c5033490a1aa023a663ec95072e0b3df4d6a7a2d96cf6f8dfe13a5fbd8ebfdbcebaa47978199  swayr-0.12.1.tar.gz
"
